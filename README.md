# prime-number-server

Prime number calculation service.

Streams prime numbers up to a given one over gRPC.

## Compile / Test / Run

To compile and run this service you need [sbt](https://www.scala-sbt.org/).
:information_source: project has been created with sbt version 1.5.5

```shell
sbt clean test
```

Environment variables used to config the service

| Name                     | Default value | Description                    |
|:-------------------------|:--------------|:-------------------------------|
| PRIME\_SERVER\_INTERFACE | 127.0.0.1     | Interface for endpoind binding |
| PRIME\_SERVER\_PORT      | 9090          | Endpoind binding port          |

Use `sbt run` to start the service and `<Ctrl>-C` to stop running one.

If you want to run service with custom binding settings use
```shell
sbt run -DPRIME_SERVER_INTEFACE=0.0.0.0 -DPRIME_SERVER_PORT=8080
```

You can use [grpcurl](https://github.com/fullstorydev/grpcurl) to test it.

```shell
grpcurl -d '{"upTo":102}' -plaintext 127.0.0.1:8080 primenumbers.PrimeNumbersService/PrimesUpTo
```

To create Docker image run

```shell
sbt docker:publishLocal
```

## Why this ALL

I have received an assigment. 
So, I thougt, it is an opportunity and one more reason to try things from my tech exploration checklist :)

gRPC, protobuf, sbt - sounds fun.

So, I thought:
Nice, now it is time to check how those are working and hopefully use them later in some projects.
I have been reading about gRPC, but have not used it so far.

I know there is akka-grpc library, so, I went straight to the [akka-grpc](https://doc.akka.io/docs/akka-grpc/current/index.html)
to read how that could help.
And nice [akka-platform-guide](https://developer.lightbend.com/docs/akka-platform-guide) provided insights into the topic.

I decided to proceed with 2 projects (as they could exist and evolve as independent pieces)
- mss-primes-playground/prime-number-server>
- mss-primes-playground/proxy-service>

Start simple and see how things could be improved.

I have chosen `sbt` as a build tool, just because I always wanted to try it, but have never had a chance besides some courses from [Coursera](https://www.coursera.org/).
It seems to be less cryptic compared to what it was when it just emerged years back!

So
```shell
$ sbt new akka/akka-grpc-quickstart.g8
$ sbt new akka/akka-http-quickstart.g8
```
gave me a base to start.

I have defined very simple protocol.

There was a statement in the assigment 'it serves responses continuously over gRPC and uses proper abstractions to communicate failure'
I kind of felt that the last part of it has its own catch.

But, as this is my first acquaintance with gRPC, I don't really know at glance what are pitfalls of akka-grpc and gRPC in general.
I thought it should be something connected to protocol validation (like arguments maybe), http timeouts, stream size limits, idle connections..

I think, it is better to have some MVP and then gradually improve it, especially when you are on the exploration route.

As the Prime numbers generation algorithm I have chosen the simplest one:
 just check if any number from 2 to square root of N is a divisor of N.

Actually, at the beginning I didn't even implement prime generation. I put odd numbers just to try how this gRPC stuff works.
Thx to the [grpcurl](https://github.com/fullstorydev/grpcurl), it was easy to "play" with.

The algorithm can be changed later, when there is a need for more efficient one (maybe using `BitInt.isProbablePrime` for some numbers or something totally different).

For the proxy, I have used [akka-http](https://doc.akka.io/docs/akka-http/current/index.html) as it works nicely with [akka-grpc](https://doc.akka.io/docs/akka-grpc/current/index.html) and [akka-streams](https://doc.akka.io/docs/akka/current/stream/index.html).
And `EntityStreamingSupport.csv()` with some `Marshaller[PrimeReply, ByteString]` felt like a natural choice to full fill output format requirement.

As I was using `sbt run` to run those 2 services, I thought, it would be easier to just use docker-compose.
I was actually happy that there is sbt-native-packager which makes it possible to create docker images from the project easily.
Simple `docker-compose.yml` and ... with `docker-compose up -d` services are ready to serve.

Solution still lacks "proper" error handling, but is already provide some value and is nice to play with.

To share project, I have chosen GitLab, because I thought:
 it is finally a good reason to try [GitLab](https://gitlab.com). I have used [Sourceforge](https://sourceforge.net/), [BitBucket](https://bitbucket.org/) and [GitHub](https://github.com/), but never [GitLab](https://gitlab.com).
I have created public group at https://gitlab.com/mss-primes-playground and pushed repositories of these two services there.

I realize, that this is not a full blown production ready solution, yet. It still requires some love and care.
But I thought it is somekind of millestone and assignemt can be send back.

### Further development
Some thoughts about further development which I hope to try at some point when free time slots pops-up.

- proxy-service
    - handles wrong inputs in a proper way
     for this I haven't yet decided on definition for "wrong input" and "proper way”
     maybe at least returning 503 when prime-number-server is not reachable may be a good candidate for
     the 1st one.
    - define OpenApi and serve it from resources
     this would give a nice documentation and possibility to try requests directly from running proxy-service instance.

- prime-numbers-server
    - proper abstraction to communicate failure
      I need to study and use gRPC more to understand and define possible failure cases

- protobuf definitions
    - put to the separate project. Use that one as dependency for code generation
      in both proxy-service and prime-numbers-server.
    - generate only Client part in proxy-service
    - generate only Server part in prime-numbers-server (maybe Client for integration tests)
    - add integration tests       

- customise docker image build
- GitLabs CI/CD
- choose and add LICENSE to the projects

So far it was great opportunity to finally get my hands on gRPC, protobufs and sbt.
Not to mention, that thanks to this assignment I finally configured my beloved mbp for Scala development with Emacs&metals
