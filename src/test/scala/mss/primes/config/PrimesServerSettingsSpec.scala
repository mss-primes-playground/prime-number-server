package mss.primes.config

import com.typesafe.config.ConfigFactory
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers


class PrimesServerSettingsSpec extends AnyFreeSpec with Matchers {

  val config = ConfigFactory.parseString("""
    |prime-number-server {
    |  interface = 0.0.0.0
    |  port = 8181
    |  parallelism = 10
    |}""".stripMargin).withFallback(ConfigFactory.defaultApplication())

  val settings = new PrimesServerSettings(config)

  "PrimesServerSettings should read correctly from Config" - {
    "interface" in {
      settings.interface shouldBe "0.0.0.0"
    }

    "port" in {
      settings.port shouldBe 8181
    }

    "parallelism" in {
      settings.parallelism shouldBe 10
    }
  }

}
