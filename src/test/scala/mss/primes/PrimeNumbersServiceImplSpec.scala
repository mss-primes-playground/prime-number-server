package mss.primes

import scala.concurrent.duration._

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.stream.testkit.scaladsl.TestSink

import mss.primes.proto.{PrimeReply, PrimesRequest}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class PrimeNumbersServiceImplSpec extends AnyFreeSpec with BeforeAndAfterAll with Matchers with ScalaFutures {

  val testKit = ActorTestKit()

  implicit val patience = PatienceConfig(scaled(5.seconds), scaled(100.millis))
  implicit val system   = testKit.system

  val service = new PrimeNumbersServiceImpl(5, system.executionContext)

  override protected def afterAll(): Unit = {
    testKit.shutdownTestKit()
  }

  "PrimeNumbersServiceImpl should generate correct" - {

    "primes for" - {

      // list taken from https://oeis.org/A000040/list
      val primes =
        List[Long](
          2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,
          61,67,71,73,79,83,89,97,101,103,107,109,113,127,
          131,137,139,149,151,157,163,167,173,179,181,191,
          193,197,199,211,223,227,229,233,239,241,251,257,
          263,269,271
        )
      for (num <- 1 to 275) {
        s"$num" in {
          val expected = primes.filter(_ <= num).map(PrimeReply(_))
          service
            .primesUpTo(PrimesRequest(num))
            .runWith(TestSink[PrimeReply]())
            .request(expected.size + 1)
            .expectNextN(expected)
            .expectComplete()
        }
      }
    }
  }
}
