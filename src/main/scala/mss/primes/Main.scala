package mss.primes

import scala.util.control.NonFatal

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import org.slf4j.LoggerFactory

object Main {

  val log = LoggerFactory.getLogger(this.getClass())

  def main(args: Array[String]): Unit = {
    val system = ActorSystem[Nothing](Behaviors.empty, "PrimesServer")
    try {
      init(system)
    } catch {
      case NonFatal(e) =>
        log.error("Initialization failed. Terminating.", e)
        system.terminate()
    }
  }

  def init(system: ActorSystem[_]): Unit = {
    val settings = config.PrimesServerSettings(system)

    val grpcService = new PrimeNumbersServiceImpl(settings.parallelism, system.executionContext)

    PrimesServer.start(system, grpcService, settings.interface, settings.port)
  }

}
