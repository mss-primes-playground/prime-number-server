package mss.primes

import scala.annotation.tailrec
import scala.collection.immutable

import akka.NotUsed
import akka.actor.typed.ActorSystem
import akka.stream.Materializer
import akka.stream.scaladsl.Source

import mss.primes.proto.{PrimeNumbersService, PrimeReply, PrimesRequest}
import org.slf4j.LoggerFactory
import scala.concurrent.Future
import scala.concurrent.ExecutionContext

class PrimeNumbersServiceImpl(parallelism: Int, ec: ExecutionContext) extends PrimeNumbersService {

  private val log = LoggerFactory.getLogger(this.getClass())

  class PrimesRange(max: Long) extends Iterator[Long] {

    private var _next: Long = 2L
    private var _hasnext: Boolean = _next <= max

    override def hasNext: Boolean = _hasnext

    override def next(): Long = {
      if(!_hasnext) Iterator.empty.next()
      val current = _next
      _hasnext = current != max
      _next = current + 1
      current
    }

  }

  /**
   * Returns prime number up to the PrimeRequest.upTo including it if prime.
   *
   * @param in request specifying the ceilling of generated stream
   */
  override def primesUpTo(in: PrimesRequest): Source[PrimeReply,NotUsed] = {
    if(in.upTo < 2) {
      Source.empty
    } else {
      Source.fromIterator(() => new PrimesRange(in.upTo))
        .mapAsync(parallelism) { number => Future {
          number -> isPrime(number)
        }(ec)
        }.collect {
          case (number, true) => PrimeReply(number)
        }
    }
  }

  /**
    * Checks if a parameter is prime by testing if any number
    * between 2 and sqrt(parameter) is a divisor of the parameter.
    *
    * @param number to check
    * @return a result of the check. True if number is prime.
    */
  private def isPrime(number: Long): Boolean = {
    if(number > 1) {
      val numberSqrt = Math.sqrt(number.toDouble).toLong

      @tailrec
      def isDivisor(divisor: Long): Boolean = {
        (divisor <= numberSqrt) &&
        (((number % divisor) == 0) || isDivisor(divisor + 1))
      }

      !isDivisor(2)
    } else {
      false
    }
  }
}
