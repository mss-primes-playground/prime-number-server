package mss.primes.config

import akka.actor.typed.ExtensionId
import akka.actor.typed.ActorSystem
import akka.actor.typed.Extension
import com.typesafe.config.Config

object PrimesServerSettings extends ExtensionId[PrimesServerSettings] {

  val ConfigKey = "prime-number-server"

  def createExtension(system: ActorSystem[_]): PrimesServerSettings = {
    new PrimesServerSettings(system.settings.config)
  }

  def get(system: ActorSystem[_]): PrimesServerSettings = apply(system)
}

class PrimesServerSettings(rootConfig: Config) extends Extension {

  val config = rootConfig.getConfig(PrimesServerSettings.ConfigKey)

  /** HTTP server inteface */
  val interface: String = config.getString("interface")

  /** HTTP server port */
  val port: Int = config.getInt("port")

  val parallelism: Int = config.getInt("parallelism")

}
