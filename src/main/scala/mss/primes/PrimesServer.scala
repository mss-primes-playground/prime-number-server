package mss.primes

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

import akka.actor.typed.ActorSystem
import akka.grpc.scaladsl.{ServerReflection, ServiceHandler}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}

import mss.primes.proto.PrimeNumbersService

object PrimesServer {

  def start(
    system: ActorSystem[_],
    grpcService: PrimeNumbersService,
    interface: String,
    port: Int
  ): Unit = {

    implicit val sys: ActorSystem[_] = system
    implicit val ec: ExecutionContextExecutor = system.executionContext

    val service: HttpRequest => Future[HttpResponse] =
      ServiceHandler.concatOrNotFound(
        proto.PrimeNumbersServiceHandler.partial(grpcService),
        ServerReflection.partial(List(proto.PrimeNumbersService))
      )

    val bound =
      Http().newServerAt(interface, port)
      .bind(service)
      .map(_.addToCoordinatedShutdown(3.seconds))

    bound.onComplete{
      case Success(binding) =>
        val address = binding.localAddress
        system.log.info("Primes Generator gRPC server {}:{}", address.getHostString, address.getPort)
      case Failure(e) =>
        system.log.error("Failed to bind gRPC endpoint. Terminating")
        system.terminate()
    }
  }

}
